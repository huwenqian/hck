package com.hck.ui;

import java.util.Scanner;

import com.hck.service.UserService;
import com.hck.service.impl.UserServiceImpl;

public class CheckOut {
	UserService u = new UserServiceImpl();
	Scanner sc = new Scanner(System.in);
	
	public void checkOut() {
		// TODO Auto-generated method stub
		System.out.println("请输入所住房间号：");
		String roomnumber = sc.next();
		System.out.println("请输入入住人身份证号：");
		String cliid = sc.next();
		int satisfaction = -1;
		float out = u.checkOut(roomnumber, cliid ,satisfaction,-1);
		if (out == 0) {
			System.out.println("退房失败");
		} else {
			System.out.println("退房成功");
			int count =(int) u.checkOut(roomnumber, cliid, satisfaction, 1);
			if(count>=5&&count < 10) {
				System.out.println("您消费达5次，享受95折优惠");
				System.out.printf("费用=%.2f\n",out*0.95);
			}else if (count>=10&&count<15) {
				System.out.println("您消费达10次，享受90折优惠");
				System.out.printf("费用=%.2f\n",out*0.90);
			}else if (count>=15) {
				System.out.println("您消费达15次，享受85折优惠");
				System.out.printf("费用=%.2f\n",out*0.85);
			}else {
				System.out.println("您消费未达5次，无优惠");
				System.out.println("费用=" + out);
			}
			System.out.println("请输入1-5的满意度");
			satisfaction = sc.nextInt();
			while(u.checkOut(roomnumber, cliid, satisfaction,-1)==1) {
				System.out.println("输入有误，请输入1-5的数");
				satisfaction = sc.nextInt();
			}
			System.out.println("评价完成，欢迎再次入住");
			
		}
		System.out.println("*************************************************");
		System.out.println("请输入x退出该操作");
	}
}
