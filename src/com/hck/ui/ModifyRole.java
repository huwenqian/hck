package com.hck.ui;

import java.util.Scanner;

import com.hck.service.UserService;
import com.hck.service.impl.UserServiceImpl;

public class ModifyRole {
	UserService u = new UserServiceImpl();
	Scanner sc = new Scanner(System.in);
	public void modifyRole() {
		System.out.println("请输入账号");
		String username = sc.next();
		System.out.println("请输入权限  0.前台管理员 1.超级管理员");
		String role = sc.next();
		if (u.modifyRole(username, role)) {
			System.out.println("修改成功");
			if(role.equals("1")) {
				System.out.println(username+"现在是超级管理员");
			}
			if(role.equals("0")) {
				System.out.println(username+"现在是前台管理员");
			}
		}else {
			System.out.println("输入账号不存在，修改失败");
		}
		
	}

}
