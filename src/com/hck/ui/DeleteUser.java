package com.hck.ui;

import java.util.Scanner;

import com.hck.service.impl.UserServiceImpl;

public class DeleteUser {
	UserServiceImpl u = new UserServiceImpl();
	Scanner sc= new Scanner(System.in);
	public void deleteUser() {
		System.out.println("请输入账号");
		String username = sc.next();
		if(u.deleteUser(username)) {
			System.out.println("删除成功");
		}else {
			System.out.println("用户不存在，删除失败");
		}
		
	}

}
