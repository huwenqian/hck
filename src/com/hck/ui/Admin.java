package com.hck.ui;

import java.io.IOException;
import java.util.Scanner;

import com.hck.service.impl.UserServiceImpl;

public class Admin {
	UserServiceImpl u =new UserServiceImpl();
	InsertUser insertUser = new InsertUser();
	InsertRoom insertRoom = new InsertRoom();
	DeleteRoom deleteRoom = new DeleteRoom();
	DeleteUser deleteUser = new DeleteUser();
	ModifyRole modifyRole = new ModifyRole();
	public void admin(String username) throws IOException, InterruptedException{
		System.out.println("欢迎" + username + "登录hck酒店管理系统");
		while (true) {
			System.out.println("*************hck酒店管理系统***************");
			System.out.println("请选择相应操作");
			Scanner scanner1 = new Scanner(System.in);
			System.out.println("1.房子 2.用户    3.管理员    4.添加管理员    5.添加房子    6.删除管理员  7.删除房子  8.修改管理员权限  9.返回");
			String choose = scanner1.next();
			switch (choose) {
			case "1":
				u.showHouse();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "2":
				u.showClient();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "3":
				u.showUser();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "4":
				insertUser.insertUser();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "5":
				insertRoom.insertRoom();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "6":
				deleteUser.deleteUser();
				scanner1.next();
				ClsUtil.cls();
			case "7":
				deleteRoom.deleteRoom();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "8":
				modifyRole.modifyRole();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "9":
				ClsUtil.cls();
				return;
			default:
				System.out.println("输入不正确");

			}
		}
	}

}
