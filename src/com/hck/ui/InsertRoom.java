package com.hck.ui;

import java.util.Scanner;

import com.hck.service.impl.UserServiceImpl;

public class InsertRoom {
	UserServiceImpl u = new UserServiceImpl();

	public void insertRoom() {
		Scanner sc = new Scanner(System.in);
		float roomprice = 0;
		System.out.println("请输入房间号");
		String roomnumber = sc.next();
		if (u.insertRoom(roomnumber, null, null, 0, 0) == true) {
			System.out.println("账号已存在");
		} else {
			System.out.println(" A.普通单人间B.豪华单人间C.普通双人间D.豪华双人间");
			System.out.println("请输入房型");
			String roomtype = sc.next();
			while (roomtype.equals("A") == false && roomtype.equals("B") == false && roomtype.equals("C") == false
					&& roomtype.equals("D")==false) {
				System.out.println("请输入ABCD");
				roomtype = sc.next();
			}
			while (true) {
				try {
					System.out.println("请输入房价");
					Scanner s = new Scanner(System.in);
					roomprice = s.nextFloat();
					break;
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("输入有误，请重新输入");
				}
			}
			if (u.insertRoom(roomnumber, roomtype, "否", roomprice, 0) == true) {
				System.out.println(roomnumber + "添加成功");
			} else {
				System.out.println("添加失败");
			}
		}
	}
}
