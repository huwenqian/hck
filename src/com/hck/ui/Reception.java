package com.hck.ui;

import java.io.IOException;
import java.util.Scanner;

public class Reception {
	RemainsHouse remain = new RemainsHouse();
	Modify modify = new Modify();
	Reservations reser = new Reservations();
	CheckHouse house = new CheckHouse();
	CheckClient client = new CheckClient();
	CheckOut out = new CheckOut();

	public void reception(String username) throws IOException, InterruptedException {
		System.out.println("欢迎" + username + "登录hck酒店管理系统");
		while (true) {
			System.out.println("*************hck酒店管理系统***************");
			System.out.println("请选择相应操作");
			Scanner scanner1 = new Scanner(System.in);
			System.out.println("1.查看房子 2.客房预定    3.查看已住客房信息    4.查看入住人信息    5.退房办理    6.退出功能操作");
			String choose = scanner1.next();
			switch (choose) {
			case "1":
				remain.remainsHouse();
				ClsUtil.cls();
				break;
			case "2":
				reser.reservations();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "3":
				house.checkHouse();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "4":
				client.checkClient();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "5":
				out.checkOut();
				scanner1.next();
				ClsUtil.cls();
				break;
			case "6":
				ClsUtil.cls();
				return;
			default:
				System.out.println("输入不正确");

			}
		}
	}

}
