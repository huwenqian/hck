package com.hck.ui;

import java.util.Scanner;

import com.hck.service.impl.UserServiceImpl;

public class DeleteRoom {
	UserServiceImpl u = new UserServiceImpl();
	Scanner sc= new Scanner(System.in);
	public void deleteRoom() {
		System.out.println("请输入房间号");
		String roomnumber = sc.next();
		if(u.deleteRoom(roomnumber)) {
			System.out.println("删除成功");
		}else {
			System.out.println("房间不存在，删除失败");
		}
		
	}
}
