package com.hck.ui;

import java.util.Scanner;

import com.hck.service.UserService;
import com.hck.service.impl.UserServiceImpl;
import java.io.IOException;

public class LoginUi {
	Modify modify = new Modify();
	CheckRole checkRole = new CheckRole();
	Reception reception = new Reception();
	Admin admin = new Admin();

	public void login() throws IOException, InterruptedException {
		int i = 0;
		while (i < 3) {
			System.err.println("*****************登录***************");
			Scanner scanner = new Scanner(System.in);
			System.out.println("请输入账号");
			String username = scanner.nextLine();
			System.out.println("请输入密码");
			String password = scanner.nextLine();
			ClsUtil.cls();
			if (modify.login(username, password) != null) {
				ClsUtil.cls();
				switch (checkRole.checkRole(username)) {
				case "0":
					reception.reception(username);
					return;
				case "1":
					admin.admin(username);
					return;
				default:
					System.out.println("非法权限");
					i = i + 1;
					break;
				}
			} else {
				System.out.println("登陆失败，用户名或密码错误");
				i = i + 1;
			}
		}
		return;

	}
}