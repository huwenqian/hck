package com.hck.dao.impl;

import java.awt.FlowLayout;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.hck.util.JdbcUtil;
import com.hck.dao.UserDao;
import com.hck.model.*;

public class UserDaoImpl implements UserDao {
//登录
	@Override
	public User login(String username, String password) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from user where username=? and password=?";
		Object[] objects = { username, password };
		try {
			User user = queryRunner.query(sql, new BeanHandler<>(User.class), objects);
			return user;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int remainsHouse(String hometype, String roomif) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from room where roomtype = ? and roomif = ?";
		try {
			List<House> ls = queryRunner.query(sql, new BeanListHandler<>(House.class), hometype, roomif);
			for (int i = 0; i < ls.size(); i++) {
				System.out.println(ls.get(i));
			}
			return ls.size();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean recordClient(String cliid, String clisex, String cliname, String cliphone) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "insert into client(cliid,clisex,cliname,cliphone) values(?,?,?,?)";
		if (checkcliid(cliid) == false) {
			try {
				int num = queryRunner.update(sql, cliid, clisex, cliname, cliphone);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean reservation(String cliid, String roomnumber, String reservedate) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		if (checkcliid(cliid) && checkroom(roomnumber) && roomIf(roomnumber, "c") != true) {
			String sql = "insert into reserve(cliid,roomnumber,reservedate) values(?,?,?)";
			try {
				int num = queryRunner.update(sql, cliid, roomnumber, reservedate);
				roomIf(roomnumber, "是");
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void checkHouse(String roomnumber) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from room where roomnumber=?";
		String sql2 = "select * from client where cliid in (select cliid from reserve where roomnumber = ?)";
		try {
			House house = queryRunner.query(sql, new BeanHandler<>(House.class), roomnumber);
			Client client = queryRunner.query(sql2, new BeanHandler<>(Client.class), roomnumber);
			System.out.println(house);
			System.out.println(client);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("不存在");
		}

	}

	@Override
	public boolean checkphone(String username, String userphone) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from user where userphone=? and username = ?";
		try {
			User user = queryRunner.query(sql, new BeanHandler<>(User.class), userphone, username);
			if (user == null) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean modifypassword(String username, String newpassword) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "update user set password =? where username=? ";
		try {
			int num = queryRunner.update(sql, newpassword, username);
			if (num != 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void checkClient(String cliid, String cliname) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from client where cliid=? and cliname = ?";
		String sql2 = "select * from room where roomnumber in (select roomnumber from reserve where cliid = ?)";
		try {
			Client client = queryRunner.query(sql, new BeanHandler<>(Client.class), cliid, cliname);
			List<House> house = queryRunner.query(sql2, new BeanListHandler<>(House.class), cliid);
			for (int i = 0; i < house.size(); i++) {
				System.out.println(house.get(i));
			}
			System.out.println(client);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean checkOut(String roomnumber, String cliid) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "delete from reserve where roomnumber=? and cliid = ?";
		try {
			int num = queryRunner.update(sql, roomnumber, cliid);
			if (num != 0) {
				roomIf(roomnumber, "否");
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkroom(String roomnumber) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from room where roomnumber=?";
		try {
			House house = queryRunner.query(sql, new BeanHandler<>(House.class), roomnumber);
			if (house == null) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean checkcliid(String cliid) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from client where cliid=?";
		try {
			Client client = queryRunner.query(sql, new BeanHandler<>(Client.class), cliid);
			if (client == null) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean roomIf(String roomnumber, String roomif) {
		// TODO Auto-generated method stub
		if (checkroom(roomnumber)) {
			if (roomif.equals("是") || roomif.equals("否")) {
				QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
				String sql = "update room set roomif =? where roomnumber=? ";
				try {
					int num = queryRunner.update(sql, roomif, roomnumber);
					if (num != 0) {
						return true;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (roomif.equals("c")) {
				QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
				String sql = "select * from room where roomnumber=?";
				try {
					House house = queryRunner.query(sql, new BeanHandler<>(House.class), roomnumber);
					if (house.getRoomif().equals("是")) {
						return true;
					} else if (house.getRoomif().equals("否")) {
						return false;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return false;
	}

	@Override
	public float roomPrice(String roomnumber) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select roomprice from room where roomnumber=?";
		try {
			Object obj = queryRunner.query(sql, new ScalarHandler(), roomnumber);
			return (float) obj;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int reservedate(String roomnumber, String cliid) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select reservedate from reserve where roomnumber=? and cliid =?";
		try {
			Object obj = queryRunner.query(sql, new ScalarHandler(), roomnumber, cliid);
			if (obj == null) {
				return -1;
			}
			return (int) obj;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean satisfaction(String roomnumber, String cliid, int satisfaction) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "insert into satisfaction(cliid,roomnumber,satisfaction) values(?,?,?)";
		try {
			int num = queryRunner.update(sql, cliid, roomnumber, satisfaction);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean roomSatisfaction(String roomnumber) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql1 = "select AVG(satisfaction) from satisfaction where roomnumber = ? group by roomnumber";
		String sql2 = "update room set satisfaction = ? where roomnumber = ?";
		try {
			Object obj = queryRunner.query(sql1, new ScalarHandler(), roomnumber);
			int num = queryRunner.update(sql2, obj, roomnumber);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long cliidcount(String cliid) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select count(*) from satisfaction where cliid = ? group by cliid";
		try {
			Object obj = queryRunner.query(sql, new ScalarHandler(), cliid);
			if (obj == null) {
				return 0;
			}
			return (long) obj;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public String checkRole(String username) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select role from user where username = ?";
		try {
			Object obj = queryRunner.query(sql, new ScalarHandler(), username);
			return (String) obj;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "0";
	}

	@Override
	public boolean checkUser(String username) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from user where username=?";
		try {
			User user = queryRunner.query(sql, new BeanHandler<>(User.class), username);
			if (user == null) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void showHouse() {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from room";
		try {
			List<House> ls = queryRunner.query(sql, new BeanListHandler<>(House.class));
			for (int i = 0; i < ls.size(); i++) {
				System.out.println(ls.get(i));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void showClient() {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from client";
		try {
			List<Client> ls = queryRunner.query(sql, new BeanListHandler<>(Client.class));
			for (int i = 0; i < ls.size(); i++) {
				System.out.println(ls.get(i));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void showUser() {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "select * from user";
		try {
			List<User> ls = queryRunner.query(sql, new BeanListHandler<>(User.class));
			for (int i = 0; i < ls.size(); i++) {
				System.out.println(ls.get(i));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean insertUser(String username, String password, String name, String userphone, String role) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "insert into user(username,password,name,userphone,role) values(?,?,?,?,?)";
		if (checkUser(username) == false) {
			try {
				int num = queryRunner.update(sql, username, password, name, userphone, role);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean deleteUser(String username) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "delete from user where username =?";
		if (checkUser(username) == true) {
			try {
				int num = queryRunner.update(sql, username);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean modifyRole(String username, String role) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "update user set role =? where username =?";
		if (checkUser(username) == true) {
			try {
				int num = queryRunner.update(sql, role, username);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean insertRoom(String roomnumber, String roomtype, String roomif, float roomprice, float satisfaction) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "insert into room(roomnumber ,roomtype ,roomif ,roomprice ,satisfaction) values(?,?,?,?,?)";
		if (checkroom(roomnumber) == false) {
			try {
				int num = queryRunner.update(sql, roomnumber, roomtype, roomif, roomprice, satisfaction);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public boolean deleteRoom(String roomnumber) {
		// TODO Auto-generated method stub
		QueryRunner queryRunner = new QueryRunner(JdbcUtil.getDataSource());
		String sql = "delete from room where roomnumber =?";
		if (checkroom(roomnumber) == true) {
			try {
				int num = queryRunner.update(sql, roomnumber);
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

}
