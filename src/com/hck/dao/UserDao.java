package com.hck.dao;

import com.hck.model.*;

public interface UserDao {
	User login(String username,String password);//用户登录
	
	int remainsHouse(String hometype,String roomif);//查房
	
	boolean recordClient(String cliid,String clisex,String cliname,String cliphone);//客户信息录入
	
	boolean reservation(String cliid,String roomnumber,String reservedate);//预定房子几天
	
	void checkHouse(String roomnumber);//查看已预定的客房信息
	
	boolean checkphone(String username,String userphone);//检查手机号是否在序列中
	
	boolean modifypassword(String username,String newpassword);//修改密码

	void checkClient(String cliid,String cliname);//查看入住人信息
	
	boolean checkOut(String roomnumber, String cliid);//退房办理
	
	boolean checkroom(String roomnumber);//检查房间号是否存在
	
	boolean checkcliid(String cliid);//检查客户id是否存在
	
	boolean roomIf(String roomnumber,String roomif);//修改或查看房间状态
	
	float roomPrice(String roomnumber); //返回房间价格
	
	int reservedate(String roomnumber, String cliid);  //返回住房时间
	
	boolean satisfaction(String roomnumber , String cliid ,int satisfaction);//添加满意度
	
	boolean roomSatisfaction(String roomnumber);//满意度写入
	
	long cliidcount(String cliid);
	
	String checkRole(String username);//检查用户权限
	
	boolean checkUser(String username);//检查管理员id是否存在
	
	void showHouse();
	
	void showClient();
	
	void showUser();
	
	boolean insertUser(String username,String password,String name,String userphone,String role);//添加管理员
	
	boolean deleteUser(String username);//删除管理员
	
	boolean modifyRole(String username , String role); //修改用户权限
	
	boolean insertRoom(String roomnumber,String roomtype,String roomif,float roomprice,float satisfaction);//添加房间
	
	boolean deleteRoom(String roomnumber);//删除房间
	
	

}
