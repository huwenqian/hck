package com.hck.util;

import java.util.ResourceBundle;

import org.apache.commons.dbcp.BasicDataSource;

public class JdbcUtil {
	private static BasicDataSource dataSource;
	private static final String driver;
	private static final String url;
	private static final String username;
	private static final String password;
	
	static {
		dataSource = new BasicDataSource();
		driver = ResourceBundle.getBundle("db").getString("driver");
		url = ResourceBundle.getBundle("db").getString("url");
		username = ResourceBundle.getBundle("db").getString("username");
		password = ResourceBundle.getBundle("db").getString("password");
		
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
	
	}
	
	public static BasicDataSource getDataSource() {
		return dataSource;
	}
}
