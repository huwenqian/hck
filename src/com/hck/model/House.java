package com.hck.model;

public class House {
	private String roomnumber;
	private String roomtype;
	private String roomif;
	private float roomprice;
	private float satisfaction;
	
	public String getRoomnumber() {
		return roomnumber;
	}

	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}

	public String getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(String roomtype) {
		this.roomtype = roomtype;
	}

	public String getRoomif() {
		return roomif;
	}

	public void setRoomif(String roomif) {
		this.roomif = roomif;
	}

	public float getRoomprice() {
		return roomprice;
	}

	public void setRoomprice(float roomprice) {
		this.roomprice = roomprice;
	}

	public float getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(float satisfaction) {
		this.satisfaction = satisfaction;
	}

	@Override
	public String toString() {
		return "客房 [客房号=" + roomnumber + ", 客房类型=" + roomtype + ", 客房是否入住=" + roomif + ", 客房价格="
				+ roomprice + "满意度 = "+ satisfaction +"]";
	}
	

	
}
