package com.hck.model;

public class Client {
	private String cliid;
	private String clisex;
	private String cliname;
	private String cliphone;

	public String getCliid() {
		return cliid;
	}
	public void setCliid(String cliid) {
		this.cliid = cliid;
	}
	public String getClisex() {
		return clisex;
	}
	public void setClisex(String clisex) {
		this.clisex = clisex;
	}
	public String getCliname() {
		return cliname;
	}
	public void setCliname(String cliname) {
		this.cliname = cliname;
	}
	public String getCliphone() {
		return cliphone;
	}
	public void setCliphone(String clicall) {
		this.cliphone = clicall;
	}
	
	@Override
	public String toString() {
		return "客户 [姓名=" + cliname + ", 性别=" + clisex + ", 身份证号=" + cliid + ", 手机号=" + cliphone
				+ "]";
	}
	
	
}
