package com.hck.model;

public class User {
	private String username;
	private String name;
	private String password;
	private String userphone;
	private String role;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserphone() {
		return userphone;
	}

	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}

	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "用户 [用户名=" + name + ", 用户账号=" + username + ", 密码=" + password + ", 手机号=" + userphone
				+ "权限"+ role+"]";
	}

	
}
