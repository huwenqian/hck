package com.hck.service.impl;

import java.sql.Date;
import java.util.Random;
import java.util.Scanner;

import javax.sound.midi.Soundbank;

import com.hck.dao.UserDao;
import com.hck.dao.impl.UserDaoImpl;
import com.hck.model.User;
import com.hck.service.UserService;

public class UserServiceImpl implements UserService {

	UserDaoImpl u = new UserDaoImpl();

	@Override
	public User login(String username, String password) {
		return u.login(username, password);
	}

	@Override
	public int remainsHouse(String hometype) {
		// TODO Auto-generated method stub
		return u.remainsHouse(hometype,"否");
	}

	@Override
	public void checkHouse(String roomnumber) {
		// TODO Auto-generated method stub
		u.checkHouse(roomnumber);

	}

	@Override
	public void checkClient(String cliid, String cliname) {
		// TODO Auto-generated method stub
		u.checkClient(cliid, cliname);
	}

	@Override
	public float checkOut(String roomnumber, String cliid, int satisfaction, int count) {
		// TODO Auto-generated method stub
		if (count == -1) {
			if (satisfaction == -1) {
				int date = u.reservedate(roomnumber, cliid);
				if (date == -1) {
					return 0;
				}
				if (u.checkOut(roomnumber, cliid)) {
						return date * u.roomPrice(roomnumber);
				} else {
					return 0;
				}
			} else if (satisfaction == 1 || satisfaction == 2 || satisfaction == 3 || satisfaction == 4
					|| satisfaction == 5) {
				u.satisfaction(roomnumber, cliid, satisfaction);
				u.roomSatisfaction(roomnumber);
				return 2;
			}
		} else if (count == 1) {
			return (int) u.cliidcount(cliid);
		}
		return 1;

	}

	@Override
	public int modifypassword(String username, String newpassword, String userphone, int rand, int code) {
		// TODO Auto-generated method stub
		// 1修改成功 2修改失败 3验证码错误 4账号电话不匹配
		if (u.checkphone(username, userphone)) {
			Random random = new Random();
			if (code == rand) {
				if (u.modifypassword(username, newpassword)) {
					return 1;
				} else {
					return 2;
				}
			} else {
				return 3;
			}
		} else {
			return 4;
		}
	}

	@Override
	public int reservation(String cliid, String clisex, String cliname, String cliphone, String roomnumber,
			String reservedate) {
		// TODO Auto-generated method stub
		if (roomnumber == null) {
			if (u.checkcliid(cliid) == true) {
				return 1;
			} else {
				return 2;
			}
		} else if (cliphone == null)
			if (u.reservation(cliid, roomnumber, reservedate) != false) {
				u.checkHouse(roomnumber);
				return 3;
			} else {
				return 4;
			}
		else {
			if (u.recordClient(cliid, clisex, cliname, cliphone) != false) {
				if (u.reservation(cliid, roomnumber, reservedate) != false) {
					u.checkHouse(roomnumber);
					return 5;
				} else {
					return 6;
				}
			} else {
				return 7;
			}
		}
	}

	@Override
	public String checkRole(String username) {
		// TODO Auto-generated method stub
		return u.checkRole(username);
	}

	@Override
	public void showHouse() {
		// TODO Auto-generated method stub
		u.showHouse();
	}

	@Override
	public void showClient() {
		// TODO Auto-generated method stub
		u.showClient();
	}

	@Override
	public void showUser() {
		// TODO Auto-generated method stub
		u.showUser();
	}

	@Override
	public boolean insertUser(String username, String password, String name, String userphone, String role) {
		// TODO Auto-generated method stub
		if(password==null) {
			return u.checkUser(username);
		}
		return u.insertUser(username, password, name, userphone, role);
	}

	@Override
	public boolean deleteUser(String username) {
		// TODO Auto-generated method stub
		return u.deleteUser(username);
	}

	@Override
	public boolean modifyRole(String username, String role) {
		// TODO Auto-generated method stub
		return u.modifyRole(username, role);
	}

	@Override
	public boolean insertRoom(String roomnumber, String roomtype, String roomif, float roomprice, float satisfaction) {
		// TODO Auto-generated method stub
		if(roomif ==null) {
			return u.checkroom(roomnumber);
		}
		return u.insertRoom(roomnumber, roomtype, roomif, roomprice, satisfaction);
	}

	@Override
	public boolean deleteRoom(String roomnumber) {
		// TODO Auto-generated method stub
		return u.deleteRoom(roomnumber);
	}
}
