package com.hck.service;

import com.hck.model.User;

public interface UserService {
User login(String username,String password);//用户登录
	
	int remainsHouse(String hometype);//剩余房量
	
	int reservation(String cliid,String clisex,String cliname,String cliphone,String roomnumber,String reservedate);//预定房子几天
	
	void checkHouse(String roomnumber);//查看已预定的客房信息

	void checkClient(String cliid,String cliname);//查看入住人信息
	
	float checkOut(String roomnumber, String cliid ,int satisfaction ,int count);//退房办理
	
	int modifypassword(String username,String newpassword ,String userphone ,int rand ,int code);//修改密码
	
	String checkRole(String username);//检查用户权限
	
	void showHouse();
	
	void showClient();
	
	void showUser();
	
	boolean insertUser(String username,String password,String name,String userphone,String role);//添加管理员
	
	boolean deleteUser(String username);//删除管理员
	
	boolean modifyRole(String username , String role); //修改用户权限
	
	boolean insertRoom(String roomnumber,String roomtype,String roomif,float roomprice,float satisfaction);//添加房间
	
	boolean deleteRoom(String roomnumber);//删除房间
	
}
